package sources;

public interface Generator {

	public double next();

}
