package sources;

public class Main {
	
	public static void main(String[] args){	
		
		MSQ msq = new MSQ();
		System.out.println("\n MSQ Results: \n");
		
		for(int i=0; i<40; i++){
			System.out.println(msq.next());
		}
		
		
		LCG lcg = new LCG();
		System.out.println("\n LCG Results: \n");
		
		for(int i=0; i<40; i++){
			System.out.println(lcg.next());
		}
	
	}

}
