package sources;

public class MSQ implements Generator{

	private int seed;
	
	public double next(){
	
		checkSeed();
		
		int number = seed*seed;
		
		this.seed = (number/100)%10000; 
		
		return (double)seed/(double)10000;
	
	}
	
	public void checkSeed(){
		
		if(seed == ((seed*seed)/100)%10000){
			seed = seed + 25;
		}
		
	}
	
	public MSQ(){
		
		seed = 6500; 
	
	}
	
	
}
