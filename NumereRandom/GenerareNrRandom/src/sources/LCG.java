package sources;

public class LCG implements Generator {

	private long seed;
	
	private int a, c;
	private int aux = 0;
	private long m;
	
	public double next(){
		
		if(seed == (a * seed + c) % m){
			seed+=7;
		}
		
		this.seed = (a * seed + c) % m;
		
		aux++;
		
		if(aux == 5){
		
			checkSeed();
			aux = 0;
		
		}
		
		return (double)(seed % m)/m;
	}
	
	public void checkSeed(){
		seed++;
	}
	
	public LCG(){
		
		seed = 6250;
		a = 3;
		c = 4;
		m = 10000;
		
	}
	
}
